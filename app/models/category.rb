class Category < ApplicationRecord
	has_many :posts, dependent: :destroy
	validates :category, presence: true,
												length: { maximum: 500}
end
